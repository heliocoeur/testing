db.createUser(
	{
		"user" : "{{ mongodb_username }}",
		"pwd" : "{{ mongodb_password }}",
		"roles" : [
			{"role" : "dbOwner", "db" : "configurator" },
            {"role" : "dbOwner", "db" : "{{ logms_dbname }}" },
            {"role" : "dbOwner", "db" : "{{ user_dbname }}" },
            {"role" : "dbOwner", "db" : "{{ motifoperationnel_dbname }}" },
            {"role" : "dbOwner", "db" : "{{ motifretard_dbname }}" },
            {"role" : "dbOwner", "db" : "{{ arrondiretard_dbname }}" },
            {"role" : "dbOwner", "db" : "{{ oauth2ms_dbname }}" }
		]
	}
)