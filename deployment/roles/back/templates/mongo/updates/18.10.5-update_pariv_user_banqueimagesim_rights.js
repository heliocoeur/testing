db = db.getSiblingDB('configurator')
db.grantRolesToUser(
   "{{ mongodb_username }}",
   [ { role: "dbOwner", db: "{{ banqueimagesim_dbname }}" } ]
)