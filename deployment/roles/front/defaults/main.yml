# Wether to run a standalone Apache container
# If set to false, won't run an Apache container
# and apache_vhosts_dir should point to a directory where it will be loaded by our Apache instance
pariv_run_standalone_apache: false

become_user_front: epviadmw

# ##
# Mutualized required config
# These configs are only used when pariv_run_standalone_apache == false
# to reload/restart the mutualized server
##

# integ: i, prod: p, etc.
environment_letter: d

# User owning the apache process (used in Apache conf)
apache_user: webbiv{{ environment_letter }}1

# trigram used for mutualization with BrokerIV front server
biv_trigram_env: biv{{ environment_letter }}

# main Apache conf file path
# by default, use the mutualized BrokerIV config file
# this parameter is ignored when using the standalone deployment,
# as a specific httpd.conf is created for the Apache container
apache_config_path: /home/epviadmw/config/httpd_biv1_RP_SSL.conf

# ##
# Standalone required config
# These configs are only used when pariv_run_standalone_apache == true
# to install and run the Apache container
##
apache_registry_url: registry.it.sncf.fr
apache_image_version: latest
apache_exposed_port: "{{ pariv_vhost_listen_port }}" # Use with standalone env (dev)
apache_container_name: pariv-apache2

# config ussed by both mutualized and standalone
# Default is for mutualized
apache_home_dir: /home/epviadmw  # Recommended standalone: /home/docker/vol/pariv_project/Apache2

###
# End mutualized/standalone configs...
###

# Folders where conf and vhosts conf are saved
apache_conf_dir: "{{ apache_home_dir }}/config"
apache_vhosts_dir: "{{ apache_conf_dir }}/vhosts"
apache_certs_dir: "{{ apache_conf_dir }}/certs"
apache_pariv_vhost_filename: pariv-vhost.conf

docker_vol_dir: /home/docker/vol
pariv_project_basepath: "{{ docker_vol_dir }}/pariv_project"

# Name of the certs to use in this env
pariv_key_filename: pariv-np.key
pariv_cert_filename: pariv-np.pem
pariv_certchain_filename: pariv-np-bundle.pem

# Name of the cert/key as copied on target machines
pariv_key_dest_filename: pariv-key.pem
pariv_cert_dest_filename: pariv-cert.pem
pariv_certchain_dest_filename: pariv-chain.pem

# Public hostname when accessing ParIV, mostly used by clients users and apps
# Behind a reverse proxy, the servername of the front server
pariv_front_servername: "{{ ansible_eth1.ipv4.address }}"

# Hostname of the server hosting ParIV middleware
# Used to build internal container-to-container communication URLs
pariv_middle_servername: "{{ hostvars[inventory_hostname].ansible_host }}"

# IP, port, certs and confs used with ParIV Apache vhost
pariv_vhost_ip: "{{ pariv_front_servername }}"
pariv_vhost_listen_port: 443
pariv_vhost_certfile_path: "{{ apache_certs_dir }}/{{ pariv_cert_dest_filename }}"
pariv_vhost_keyfile_path: "{{ apache_certs_dir }}/{{ pariv_key_dest_filename }}"
pariv_vhost_chainfile_path: "{{ apache_certs_dir }}/{{ pariv_certchain_dest_filename }}"
pariv_vhost_sslengine: "on" # "on" or "off" with quotes otherwise Ansible will convert it as "True" or "False"

# Same ports as in par-iv role...
# Duplicate vars but makes it easier to override!
pariv_exposed_port: 8011
pariv_hostname: "{{ pariv_middle_servername }}"
pariv_proxypass: "http://{{ pariv_hostname }}:{{ pariv_exposed_port }}"

user_exposed_port: 8012
user_hostname: "{{ pariv_middle_servername }}"
user_proxypass: "http://{{ user_hostname }}:{{ user_exposed_port }}"

arrondiretard_exposed_port: 8013
arrondiretard_hostname: "{{ pariv_middle_servername }}"
arrondiretard_proxypass: "http://{{ arrondiretard_hostname }}:{{ arrondiretard_exposed_port }}"

motifoperationnel_exposed_port: 8014
motifoperationnel_hostname: "{{ pariv_middle_servername }}"
motifoperationnel_proxypass: "http://{{ motifoperationnel_hostname }}:{{ motifoperationnel_exposed_port }}"

motifretard_exposed_port: 8016
motifretard_hostname: "{{ pariv_middle_servername }}"
motifretard_proxypass: "http://{{ motifretard_hostname }}:{{ motifretard_exposed_port }}"

oauth2ms_exposed_port: 8083
oauth2ms_hostname: "{{ pariv_middle_servername }}"
oauth2ms_proxypass: "http://{{ oauth2ms_hostname }}:{{ oauth2ms_exposed_port }}"

logms_exposed_http_port: 8015
logms_hostname: "{{ pariv_middle_servername }}"
logms_proxypass: "http://{{ logms_hostname }}:{{ logms_exposed_http_port }}"

banqueimagesim_exposed_port: 8017
banqueimagesim_hostname: "{{ pariv_middle_servername }}"
banqueimagesim_proxypass: "http://{{ banqueimagesim_hostname }}:{{ banqueimagesim_exposed_port }}"