# Config file used when deploying ParIV on a standalone environment
# This config is used with a standalone Apache2 container
# When deploying on a mutualized environment, only the VHost is copied on target machine,
# and an already existing Apacghe config is used


# Module
LoadModule authz_core_module /usr/lib/apache2/modules/mod_authz_core.so
LoadModule authz_host_module /usr/lib/apache2/modules/mod_authz_host.so
LoadModule auth_basic_module /usr/lib/apache2/modules/mod_auth_basic.so
LoadModule mime_module /usr/lib/apache2/modules/mod_mime.so
LoadModule dir_module /usr/lib/apache2/modules/mod_dir.so
LoadModule alias_module /usr/lib/apache2/modules/mod_alias.so
LoadModule deflate_module /usr/lib/apache2/modules/mod_deflate.so
LoadModule headers_module /usr/lib/apache2/modules/mod_headers.so
LoadModule include_module /usr/lib/apache2/modules/mod_include.so
LoadModule lbmethod_byrequests_module /usr/lib/apache2/modules/mod_lbmethod_byrequests.so
LoadModule negotiation_module /usr/lib/apache2/modules/mod_negotiation.so
LoadModule proxy_module /usr/lib/apache2/modules/mod_proxy.so
LoadModule proxy_balancer_module /usr/lib/apache2/modules/mod_proxy_balancer.so
LoadModule proxy_connect_module /usr/lib/apache2/modules/mod_proxy_connect.so
LoadModule proxy_http_module /usr/lib/apache2/modules/mod_proxy_http.so
LoadModule rewrite_module /usr/lib/apache2/modules/mod_rewrite.so
LoadModule setenvif_module /usr/lib/apache2/modules/mod_setenvif.so
LoadModule slotmem_shm_module /usr/lib/apache2/modules/mod_slotmem_shm.so
LoadModule unique_id_module /usr/lib/apache2/modules/mod_unique_id.so
LoadModule ssl_module /usr/lib/apache2/modules/mod_ssl.so
LoadModule socache_shmcb_module /usr/lib/apache2/modules/mod_socache_shmcb.so
LoadModule mpm_prefork_module /usr/lib/apache2/modules/mod_mpm_prefork.so

# Generaux
ServerRoot "/home/web000p1"
ServerName docker

Listen {{ pariv_vhost_listen_port }} ssl-cache

User "web000p1"
Group "web000p1"
DocumentRoot "/home/web000p1/inst001/htdocs"

# Tunning
Timeout 50
KeepAliveTimeout 8
StartServers 10
ServerLimit 256
MaxClients 50
MaxRequestsPerChild 70

<IfModule mpm_prefork_module>
</IfModule>

<IfModule mpm_worker_module>
</IfModule>

# LOG
LogLevel error
LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
LogFormat "%h %l %u %t \"%r\" %>s %b" common
<IfModule logio_module>
LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
</IfModule>

CustomLog "/home/web000p1/inst001/logs/access_log" combined
ErrorLog "/home/web000p1/inst001/logs/error_log"

# RUN
PIDFile /home/web000p1/inst001/run/httpd.pid
Mutex file:/home/web000p1/inst001/run default ssl-cache
DefaultRuntimeDir /home/web000p1/inst001/run

# Sécurité
ServerTokens Prod
ServerSignature off
TraceEnable off
HostnameLookups off

TypesConfig inst001/conf/mime.types
DirectoryIndex index.htm index.html default.html index.cgi
AddType application/x-compress .Z
AddType application/x-gzip .gz .tgz

# ssl
SSLProtocol               -All +TLSv1 +TLSv1.1 +TLSv1.2
SSLCipherSuite             ALL:!aNULL:!ADH:!eNULL:!LOW:!EXP:HIGH:MEDIUM
SSLHonorCipherOrder  On
SSLInsecureRenegotiation off
#SSLCertificateFile              /home/web000p1/inst001/certificats/certificat.crt
#SSLCertificateKeyFile           /home/web000p1/inst001/certificats/certificat.key

SSLProxyEngine                  on
SSLOptions                      +StrictRequire
SSLPassPhraseDialog             builtin
SSLSessionCache                 shmcb:/home/web000p1/inst001/run/ssl_scache(512000)
SSLSessionCacheTimeout          3600
SSLRandomSeed                   startup builtin
SSLRandomSeed                   connect builtin

IncludeOptional /home/web000p1/inst001/conf/virtualhost/pariv-vhost.conf

# DocRoot
ScriptAlias /cgi-bin/ "/home/web000p1/inst001/cgi-bin/"